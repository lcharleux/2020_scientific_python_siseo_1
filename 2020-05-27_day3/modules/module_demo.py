# A SIMPLE MODULE
import numpy as np

def func(x):
    """
    A function.
    """
    return np.sin(x)


a = 5